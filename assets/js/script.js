// open Search block  for mobile versions

const headerFunctions = {
  toggleOpenSearch: (event, elem) => {
    event.stopPropagation()
    const parent = elem.closest(".P-search-block")
    parent.classList.toggle('P-active-search')
    parent.classList.toggle('I-active')
  },

  toggleMenu: () => {
    document.getElementsByClassName('P-burger')[0].classList.toggle('P-open')
    document.getElementsByClassName('P-bg-menu')[0].classList.toggle('P-open')
    document.getElementsByClassName('P-menu-list-block')[0].classList.toggle('P-open')
  },

}


const clickOutside = {

  closeOpenedBlocks: () => {
    const openedList = document.getElementsByClassName('I-active');
    for (let i = 0; i < openedList.length; i++) {
      openedList[i].classList.remove('P-active-search')
      openedList[i].classList.remove('I-active')
    }
  },
  searchBlock: (e) => {
    e.stopPropagation()
  }
}

document.addEventListener('click', clickOutside.closeOpenedBlocks)


// Page loader

const pageLoader = {
  animationEnd: () => {
    const pageLoader = document.getElementsByClassName('G-page-loader')[0];
    pageLoader.classList.add('P-animation-end');
  }
}

setTimeout(() => {
  pageLoader.animationEnd()
}, 1000)


// Animation  fadeIn and fadeOut

const animations = {
  fadeInEffect: (elem) => {
    let opacity = 0
    const fadeEffect = setInterval(function () {
      if (opacity < 1) {
        opacity += 0.05;
        elem.style.opacity = opacity;
      } else {
        clearInterval(fadeEffect);
      }
    }, 50);
  },

  fadeOutEffect: (elem) => {
    let opacity = +elem.style.opacity
    const fadeEffect = setInterval(function () {
      if (opacity >= 0) {
        opacity -= 0.05;
        elem.style.opacity = opacity;
      } else {
        clearInterval(fadeEffect);
      }
    }, 50);
  }
}


// Comments link  block select

const commentsSection = {
  selectLikes: (value) => {
    const likesBoxes = document.querySelectorAll('.P-most-like-box');
    const likesButtons = document.querySelectorAll('.P-likes-js');
    for (let i = 0; i < likesBoxes.length; i++) {
      if (+likesBoxes[i].dataset.id === value) {
        likesBoxes[i].classList.add('P-active-likes')
        animations.fadeInEffect(likesBoxes[i])
      } else {
        likesBoxes[i].classList.remove('P-active-likes')
        animations.fadeOutEffect(likesBoxes[i])
      }
    }
    for (let i = 0; i < likesButtons.length; i++) {
      if (+likesButtons[i].dataset.id === value) {
        likesButtons[i].classList.add('P-active-likes')
      } else {
        likesButtons[i].classList.remove('P-active-likes')
      }
    }

  }
}

commentsSection.selectLikes(1)

const scrollTopButton = {
  showScrollButton: () => {
    const scrollButton = document.getElementsByClassName('G-scroll-top')[0];
    if (scrollButton && window.scrollY > 100) {
      scrollButton.classList.add('G-show-scroll')
    } else {
      scrollButton.classList.remove('G-show-scroll')

    }
  },
  scrollTop: () => {
    window.scrollTo(0, 0)
  }
}
window.addEventListener('scroll', scrollTopButton.showScrollButton)


//  Cover section slider

$('.P-cover-slider').slick({
  dots: false,
  infinite: true,
  arrows: true,
  speed: 600,
  autoplay: true,
  fade: true,
  slidesToShow: 1,
  slidesToScroll: 1,
})

//  Cover section slider

$('.P-articles-slider').slick({
  dots: false,
  infinite: true,
  arrows: true,
  speed: 600,
  autoplay: true,
  slidesToShow: 3,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
      }
    },
    {
      breakpoint: 766,
      settings: {
        slidesToShow: 1,
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
})
